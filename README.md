# European Commission Fonts

This is a repository of [Nix package expressions][nix homepage] for the
corporate fonts used at European Commission.

These fonts are not open-source and cannot be distributed freely, they are for
internal use only. Check the [terms and conditions][terms and conditions].

External contractors must sign a specific [acceptance form][acceptance form] to
use them on their workstation.

## Usage

To use the font in LaTeX, the procedure may vary depending on the LaTeX engine.
In the following table, you'll find the font name to use depending on the LaTeX
engine in use.

| Fonts                       | PDFLatex     | LuaLatex                       |
| --------------------------- | ------------ | ------------------------------ |
| ECSquareSansPro-Bold        | rfecsquarebd | EC Square Sans Pro Bold        |
| ECSquareSansPro-BoldItalic  | rfecsquarebi | EC Square Sans Pro BoldItalic  |
| ECSquareSansPro-Italic      | rfecsquareit | EC Square Sans Pro Italic      |
| ECSquareSansPro-Light       | rfecsquarelg | EC Square Sans Pro Light       |
| ECSquareSansPro-LightItalic | rfecsquareli | EC Square Sans Pro LightItalic |
| ECSquareSansPro-MedItalic   | rfecsquaremi | EC Square Sans Pro MedItalic   |
| ECSquareSansPro-Medium      | rfecsquareme | EC Square Sans Pro Medium      |
| ECSquareSansPro-Regular     | rfecsquarere | EC Square Sans Pro Regular     |
| ECSquareSansPro-Thin        | rfecsquareth | EC Square Sans Pro Thin        |
| ECSquareSansPro-ThinItalic  | rfecsquareti | EC Square Sans Pro ThinItalic  |
| ECSquareSansPro-XBlack      | rfecsquarexb | EC Square Sans Pro XBlack      |
| ECSquareSansPro-XBlackItal  | rfecsquarexi | EC Square Sans Pro BlackItal   |

> **Note:** Fonts are available in TrueType and OpenType

### LuaLatex

To use this engine, only the file `EC SQUARE SANS 2.006.zip` is required.

```tex
\usepackage{ecsquare}
```

### PDFLatex

To use this engine, two files are required: `EC SQUARE SANS 2.006.zip` and
`EC-Square-Sans-Pro-LaTeX.zip`.

```tex
\usepackage{ecsquare}
```

## Installation

Since the installation process cannot be automated for legal reasons, the font
files needs to be downloaded manually and added to the Nix store. The
recommended LaTeX engine is LuaLatex.

1. Download the files `EC SQUARE SANS 2.006.zip` and optionally
   `EC Square Sans Pro LaTeX.zip` if you use the PDFLatex engine.
2. Rename them to `EC-SQUARE-SANS-2.006.zip` and `EC-Square-Sans-Pro-LaTeX.zip`
   respectively
3. Add them to the Nix store
   - `nix-prefetch-url file:///path/to/EC-SQUARE-SANS-2.006.zip`
   - (optional) `nix-prefetch-url file:///path/to/EC-Square-Sans-Pro-LaTeX.zip`

> **Warning:** The path to the file might vary, so please, adapt the command
> accordingly

## API

This package is contains a `flake.nix` which exposes its derivations in an
[overlay][nix overlays].

Exposed derivations:

- `ec-square-sans`: The EC Square Sans Pro font made for being installed system
  wide on the host system.
- `ec-square-sans-lualatex`: The EC Square Sans Pro font for LaTeX (LuaLatex).
- `ec-square-sans-pdflatex`: (deprecated) The EC Square Sans Pro font for LaTeX
  (PDFLatex). This derivation requires an extra archive
  `EC Square Sans Pro LaTeX.zip` which contains all the files the PDFLatex
  engine requires.

> **Warning:** When using LaTeX, do not use both package, choose
> `ec-square-sans-lualatex` or `ec-square-sans-pdflatex` but not both.

To use it in your own package, here's a minimum working example:

```nix
{
  description = "Simple flake with EC Square Sans fonts";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    ec-fonts.url = "git+https://code.europa.eu/pol/ec-fonts/";
  };

  outputs = { self, nixpkgs, flake-utils, ec-fonts, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          overlays = [
            ec-fonts.overlays.default
          ];

          inherit system;
        };

        tex = pkgs.texlive.combine {
          inherit (pkgs.texlive) scheme-full latex-bin latexmk;

          ec-fonts = {
            pkgs = [ pkgs.ec-square-sans-lualatex ];
          };
        };
      in
      {
        devShells.default = pkgs.mkShellNoCC {
          name = "devshell";

          buildInputs = [
            tex
          ];
        };
      });
}
```

[nix homepage]: https://nixos.org
[terms and conditions]:
  https://myintracomm.ec.europa.eu/corp/comm/VisualIdentity/Pages/Rules.aspx
[acceptance form]:
  https://myintracomm.ec.europa.eu/corp/comm/VisualIdentity/Documents/Rules/EcSquareSansProFont/form_ec_square_sans_pro_for_contractors-en.pdf
[nix overlays]: https://nixos.wiki/wiki/Overlays
