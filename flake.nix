{
  description = "European Commission Fonts";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
  };

  outputs = inputs @ {
    flake-parts,
    nixpkgs,
    self,
    ...
  }:
    flake-parts.lib.mkFlake {inherit inputs;} {
      systems = ["x86_64-linux" "aarch64-darwin"];

      perSystem = {
        config,
        self',
        inputs',
        pkgs,
        system,
        ...
      }: let
        pkgs = import nixpkgs {
          overlays = [
            self.overlays.default
          ];
          inherit system;
        };

        tex = pkgs.texlive.combine {
          inherit (pkgs.texlive) scheme-full latex-bin latexmk;
          inherit (pkgs) ec-square-sans-pdflatex ec-square-sans-lualatex;
        };
      in {
        formatter = pkgs.alejandra;

        packages.ec-square-sans = pkgs.ec-square-sans;
        packages.ec-square-sans-pdflatex = pkgs.ec-square-sans-pdflatex;
        packages.ec-square-sans-lualatex = pkgs.ec-square-sans-lualatex;

        # Nix develop
        devShells.default = pkgs.mkShellNoCC {
          name = "ec-fonts-devshell";

          buildInputs = [
            tex
          ];
        };

        checks.ec-square-sans = pkgs.ec-square-sans;
        checks.ec-square-sans-pdflatex = pkgs.ec-square-sans-pdflatex;
        checks.ec-square-sans-lualatex = pkgs.ec-square-sans-lualatex;
      };

      flake = let
        messageBuilder = name: ''
          Please note that this font is for internal use only.

          Check the terms and conditions at https://myintracomm.ec.europa.eu/corp/comm/VisualIdentity/Pages/Rules.aspx.
          External contractors have to sign a specific acceptance form: https://myintracomm.ec.europa.eu/corp/comm/VisualIdentity/Documents/Rules/EcSquareSansProFont/form_ec_square_sans_pro_for_contractors-en.pdf.
          Therefore, in order to use the font, you must accept the terms
          and conditions at https://myintracomm.ec.europa.eu/corp/comm/VisualIdentity/Documents/Rules/ec_square_sans_pro_licence_terms.pdf
          and download it from: https://webgate.ec.europa.eu/connected/docs/DOC-173180.

          Once the file is downloaded, rename it to ${name} and then
          add it to the Nix store with the following command:
          nix-prefetch-url file:///path/to/${name}
        '';

        overlay-ec-fonts = nixpkgs: final: prev: {
          ec-square-sans-lualatex = let
            ec-square-sans = (self.overlays.default final prev).ec-square-sans;
          in
            final.stdenvNoCC.mkDerivation (finalAttrs: {
              name = "ec-square-sans-lualatex";
              pname = "ec-square-sans-lualatex";
              version = "1.0.0.";

              src = ./src;

              dontConfigure = true;
              nativeBuildInputs = [ec-square-sans final.unzip];
              sourceRoot = ".";

              installPhase = ''
                runHook preInstall

                # Copy from ec-square-sans
                install -m644 -D ${ec-square-sans}/share/fonts/truetype/ec-square-sans/*.ttf --target $out/fonts/truetype/ec-square-sans/
                install -m644 -D ${ec-square-sans}/share/fonts/opentype/ec-square-sans/*.otf --target $out/fonts/opentype/ec-square-sans/

                install -m644 -D $src/tex/lualatex/ecsquare/ecsquare.sty --target $out/tex/lualatex/ec-square-sans/

                runHook postInstall
              '';

              passthru = {
                pkgs = [finalAttrs.finalPackage];
                tlType = "run";
              };

              meta = {
                homepage = "https://myintracomm.ec.europa.eu/corp/comm/VisualIdentity/Pages/Rules.aspx";
                description = "The EC Square Sans Pro font for LaTeX only";
              };
            });

          ec-square-sans-pdflatex = let
            ec-square-sans = (self.overlays.default final prev).ec-square-sans;
          in
            final.stdenvNoCC.mkDerivation (finalAttrs: {
              name = "ec-square-sans-pdflatex";
              pname = "ec-square-sans-pdflatex";
              version = "1.0.0.";

              src = [
                (final.requireFile rec {
                  name = "EC-Square-Sans-Pro-LaTeX.zip";
                  sha256 = "0ls84jy517l9iwy8fpgfvr43kw8dim1ad8gkz7kw4k0ip33sd9cn";
                  message = messageBuilder name;
                })
                ./src
              ];

              dontConfigure = true;
              nativeBuildInputs = [ec-square-sans final.unzip];
              sourceRoot = ".";

              installPhase = ''
                runHook preInstall

                # Copy from ec-square-sans
                install -m644 -D ${ec-square-sans}/share/fonts/truetype/ec-square-sans/*.ttf --target $out/fonts/truetype/ec-square-sans/
                install -m644 -D ${ec-square-sans}/share/fonts/opentype/ec-square-sans/*.otf --target $out/fonts/opentype/ec-square-sans/

                # Copy for LaTeX
                install -m644 -D afm/*.afm --target $out/fonts/afm/ec-square-sans/
                install -m644 -D tfm/*.tfm --target $out/fonts/tfm/ec-square-sans/
                install -m644 -D vf/*.vf --target $out/fonts/vf/ec-square-sans/

                install -m644 -D *.map --target $out/fonts/map/pdftex/ec-square-sans
                install -m644 -D t1ecsquare.fd --target $out/tex/latex/ec-square-sans/
                install -m644 -D $src/tex/latex/ecsquare/ecsquare.sty --target $out/tex/latex/ec-square-sans/

                runHook postInstall
              '';

              passthru = {
                pkgs = [finalAttrs.finalPackage];
                tlType = "run";
              };

              meta = {
                homepage = "https://myintracomm.ec.europa.eu/corp/comm/VisualIdentity/Pages/Rules.aspx";
                description = "The EC Square Sans Pro font for LaTeX only";
              };
            });

          ec-square-sans = final.stdenvNoCC.mkDerivation (finalAttrs: {
            name = "ec-square-sans";
            pname = "ec-square-sans";

            src = final.requireFile rec {
              name = "EC-SQUARE-SANS-2.006.zip";
              sha256 = "0zsy4dqq5d4f424mi4li2sawfn8qpmmm6p9acibbj60vdnn2y3f9";
              message = messageBuilder name;
            };

            dontConfigure = true;
            nativeBuildInputs = [final.unzip];
            sourceRoot = ".";

            installPhase = ''
              runHook preInstall

              install -m644 -D "EC SQUARE SANS 2.006/DESKTOP/TRUETYPE (ttf)/"*.ttf --target $out/share/fonts/truetype/ec-square-sans/
              install -m644 -D "EC SQUARE SANS 2.006/DESKTOP/OPENTYPE (otf)/"*.otf --target $out/share/fonts/opentype/ec-square-sans/

              runHook postInstall
            '';

            meta = {
              homepage = "https://myintracomm.ec.europa.eu/corp/comm/VisualIdentity/Pages/Rules.aspx";
              description = "The EC Square Sans Pro font";
            };
          });
        };
      in {
        overlays.default = overlay-ec-fonts nixpkgs;
      };
    };
}
